
Installing this project:

You can get all the info you need here: https://docs.vapid.com/#install

But for now, run this:

```
npm install -g @vapid/cli
```

Then you can run this project, after cloning it:

```
vapid start .
```